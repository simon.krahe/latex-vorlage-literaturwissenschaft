Grundlegende Infos zu LaTeX sind im PDF. Man sollte das PDF und die main.tex-Datei (den Code) parallel geöffnet haben, um zu verstehen was gemeint ist.

Alternativ einfach hier angucken: https://www.overleaf.com/read/ntgsypmcqvzk
Overleaf habe ich auch selber genutzt. Wenn ihr die Vorlage dort selbst verwenden wollt, einfach den Inhalt von main.tex in eine neue Datei kopieren und die englisch.bib Datei dazu hochladen.
